
![Lang](https://img.shields.io/badge/Language-Ruby-red)
[![Build Status](https://travis-ci.com/rokhimin/Rails-Mahad.svg?branch=master)](https://travis-ci.com/rokhimin/Rails-Mahad)

# Mahad
Mahad is rails app for testing webhooks discord, slack
> https://mahad.herokuapp.com

# Using

## get url webhook discord?
open discord => your server => server settings => webhooks

![](https://i.imgur.com/LGQsTHK.png)

## get url webhook slack?
> https://api.slack.com/apps

![](https://i.imgur.com/kndXr31.png)

# License
MIT License.